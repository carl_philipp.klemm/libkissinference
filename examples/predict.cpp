//
// libkissinference - an inference libary for kiss networks
// Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
//
// This file is part of libkissinference.
//
// libkissinference is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// libkissinference is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
//

#include "eistype.h"
#include <execution>
#include <iostream>
#include <filesystem>
#include <algorithm>
#include <system_error>
#include <condition_variable>
#include <mutex>
#include <algorithm>

#include "kissinference/kissinference.h"

#define PASS_STR "Pass"

static std::vector<std::filesystem::path> dirs;
std::mutex printmtx;
long long passIndex = -1;

#ifdef WINDOWS
#define OUTSTREAM std::wcout
#else
#define OUTSTREAM std::cout
#endif

static std::vector<std::filesystem::path> getFiles(const std::filesystem::path dir, std::string extension = "")
{
	std::vector<std::filesystem::path> out;
	if(!std::filesystem::is_directory(dir))
	{
		if(extension.empty() || dir.extension() == extension)
			out.push_back(dir);
		return out;
	}

	for(const std::filesystem::directory_entry& dirent : std::filesystem::directory_iterator{dir})
	{
		if(!extension.empty() && dirent.path().extension() != extension)
			continue;
		out.push_back(dirent.path());
	}
	return out;
}

static bool checkDir(const std::filesystem::path& outDir)
{
	if(!std::filesystem::is_directory(outDir))
	{
		if(!std::filesystem::create_directory(outDir))
		{
			OUTSTREAM<<outDir<<" dose not exist and can not be created\n";
			return false;
		}
	}
	return true;
}

struct Request
{
	std::filesystem::path path;
	std::condition_variable cond;
	std::mutex mutex;
};

#ifdef WINDOWS
static std::wstring stringConvert(const std::string& in)
{
	std::wstring out;
	out.reserve(in.size());
	for(char ch : in)
		out.push_back(static_cast<wchar_t>(ch));
	return out;
}
#else
inline static const std::string& stringConvert(const std::string& in)
{
	return in;
}
#endif

static void resultCallback(float* data, struct kiss_network* net, void* userData)
{
	Request* rq = static_cast<Request*>(userData);

	if(!data)
	{
		OUTSTREAM<<"Inference Error: "<<kiss_get_strerror(net)<<'\n';
		rq->cond.notify_all();
		return;
	}

	printmtx.lock();
	OUTSTREAM<<"\nResult for "<<rq->path<<'\n';
	OUTSTREAM<<"Outputs:\nNumber\tName\tValue\n";
	for(size_t i = 0; i < net->output_size; ++i)
		OUTSTREAM<<i<<'\t'<<stringConvert(net->output_labels[i])<<'\t'<<data[i]<<'\n';
	printmtx.unlock();

	free(data);

	rq->cond.notify_all();
}

static void pipeline(const std::filesystem::path& path, struct kiss_network* net)
{
	bool ret;

	try
	{
		EisSpectra spectra = EisSpectra::loadFromDisk(path);

		Request rq;
		rq.path = path;

		std::unique_lock<std::mutex> lk(rq.mutex);

		if(spectra.labels.size() != spectra.labelNames.size())
		{
			printmtx.lock();
			OUTSTREAM<<"Could not run inference on "<<path<<": "<<kiss_get_strerror(net)<<" a lable name is not avialble for eatch lable";
			printmtx.unlock();
			return;
		}

		struct kiss_vector *inputs = new kiss_vector[2+spectra.labels.size()];
		std::vector<float> real;
		std::vector<float> imag;
		for(size_t i = 0; i < spectra.data.size(); ++i)
		{
			real.push_back(spectra.data[i].im.real());
			imag.push_back(spectra.data[i].im.imag());
		}
		inputs[0].name = "EIS,real";
		inputs[0].values = real.data();
		inputs[0].length = real.size();

		inputs[1].name = "EIS,imaginary";
		inputs[1].values = imag.data();
		inputs[1].length = imag.size();

		for(size_t i = 0; i < spectra.labels.size(); ++i)
		{
			inputs[i+2].name = spectra.labelNames[i].c_str();
			inputs[i+2].values = new float;
			*inputs[i+2].values = spectra.labels[i];
			inputs[i+2].length = 1;
		}

		ret = kiss_async_run_inference(net, inputs, 2+spectra.labels.size(), &rq);
		for(size_t i = 0; i < spectra.labels.size(); ++i)
			delete inputs[i+2].values;
		delete[] inputs;
		if(!ret)
		{
			printmtx.lock();
			OUTSTREAM<<"Could not run inference on "<<path<<": "<<kiss_get_strerror(net)<<'\n';
			printmtx.unlock();
		}
		else
		{
			rq.cond.wait(lk);
		}
	}
	catch(const file_error& err)
	{
		OUTSTREAM<<"Could not load: "<<err.what()<<'\n';
		return;
	}
}

#ifdef WINDOWS
int wmain(int argc, wchar_t** argv)
#else
int main(int argc, char** argv)
#endif
{
	if(argc < 3)
	{
		if(argc > 0)
			OUTSTREAM<<"Usage: "<<argv[0]<<" [NETWORK] [CSV_FILE_OR_DIRECTORY]"<<'\n';
		return 1;
	}

	std::vector<std::filesystem::path> files = getFiles(argv[2], ".csv");
	if(files.empty())
	{
		OUTSTREAM<<"Could not find any .csv files in "<<argv[2]<<'\n';
		return 1;
	}

	struct kiss_network* net = kiss_load_network(reinterpret_cast<const char*>(argv[1]), resultCallback, false);
	if(!net)
	{
		OUTSTREAM<<"Could not load network from "<<argv[1]<<'\n';
		return 1;
	}
	else if(!net->ready)
	{
		OUTSTREAM<<"Could not load network: "<<kiss_get_strerror(net)<<'\n';
		kiss_free_network(net);
		return 1;
	}


	bool ret = true;
	if(argc > 3)
		ret = checkDir(argv[3]);
	if(!ret)
	{
		OUTSTREAM<<"Could not create output folders at "<<argv[3]<<'\n';
		return 1;
	}

	OUTSTREAM<<"Network has the following outputs:\n";
	for(size_t i = 0; i < net->output_size; ++i)
	{
		OUTSTREAM<<net->output_labels[i]<<'\n';
		if(std::string(net->output_labels[i]) == PASS_STR)
			passIndex = i;
	}

	std::for_each(std::execution::seq, files.begin(), files.end(), [&net](std::filesystem::path& path){pipeline(path, net);});

	kiss_free_network(net);

	return 0;
}
