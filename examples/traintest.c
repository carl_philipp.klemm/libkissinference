/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <threads.h>

#include "kissinference/kissinference.h"
#include "kissinference/train.h"

static void print_network_info(const struct kiss_network *network)
{
	printf("\tPurpose: %s\n", network->purpose);
	printf("\tInput Label: %s\n", network->inputs[0].name);
	printf("\tInput Size: %zu\n", network->inputs[0].length);
	printf("\tOutput Labels:\n");
	for(size_t i = 0; i < network->output_size; ++i)
		printf("\t\t%zu: %s\n", i, network->output_labels[i]);
	printf("\tOutput Size: %zu\n", network->output_size);
	printf("\tSoftmax: %s\n", network->softmax ? "True" : "False");
}

static struct kiss_training_data *datafeeder(struct kiss_training_session *session, void *user_ptr)
{
	printf("%s\n", __func__);
	struct kiss_training_data *data = malloc(sizeof(*data));
	data->in = kiss_create_range(0, 1, session->net->inputs[0].length*2, true);
	data->in_size = session->net->inputs[0].length;
	data->out = kiss_create_range(0, 1, session->net->output_size*2, true);
	data->out_size = session->net->output_size;

	return data;
}

static void datafreeer(struct kiss_training_data *data, void *usr_ptr)
{
	printf("%s\n", __func__);
	free(data->in);
	free(data->out);
	free(data);
}

static void progress(struct kiss_training_session *session, kiss_train_state_t state, float loss, int step, void *usr_ptr)
{
	cnd_t *cond = (cnd_t*)usr_ptr;

	printf("%s: %f %i\n", kiss_state_to_str(state), loss, step);

	if(state == KISS_STEP_COMPLETED || state == KISS_ERROR_STATE) {
		if(state == KISS_ERROR_STATE)
			printf("%s: %s\n", kiss_state_to_str(state), kiss_train_get_strerror(session));
		cnd_broadcast(cond);
	}
}

int main(int argc, char** argv)
{
	if(argc < 2) {
		printf("Usage: %s [TRAINING_ARCHIVE]\n", argc > 0 ? argv[0] : "kissinference_traintest");
		return 1;
	}

	struct kiss_training_session *session = kiss_create_training_session(argv[1], 2);
	if(!session || !session->ready) {
		printf("Unable to create session: %s\n", kiss_train_get_strerror(session));
		return 1;
	}
	puts("Training session created");

	cnd_t cond;
	int ret = cnd_init(&cond);
	if(ret != thrd_success)
	{
		puts("unable to create iso thread condition");
		return 3;
	}

	mtx_t mutex;
	ret = mtx_init(&mutex, mtx_plain);
	if(ret != thrd_success)
	{
		puts("unable to create iso thread mutex");
		return 3;
	}
	puts("Mutex setup");

	print_network_info(session->net);

	ret = kiss_start_training(session, KISS_LOSS_NNL, &datafeeder,
							&datafreeer, &progress, &cond);
	if(!ret) {
		printf("Unable to start training %s\n", kiss_train_get_strerror(session));
		kiss_free_training_session(session);
		return 1;
	}
	puts("Training started");

	puts("Awating training thread");
	mtx_lock(&mutex);
	cnd_wait(&cond, &mutex);
	mtx_unlock(&mutex);

	cnd_destroy(&cond);
	mtx_destroy(&mutex);

	return 0;
}
