/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <kissinference/kissinference.h>

#ifdef WINDOWS
static bool print_network_info(const wchar_t* filename)
#else
static bool print_network_info(const char* filename)
#endif
{
	struct kiss_network* network = kiss_load_network(filename, NULL, false);

	if(!network) {
		printf("Could not load network from %s\n", filename);
		return false;
	} else if(!network->ready) {
		printf("Could not load network from %s: %s\n", filename, kiss_get_strerror(network));
		return false;
	}

	printf("%s:\n", filename);
	printf("\tPurpose: %s\n", network->purpose);
	printf("\tInputs %zu:\n", network->input_count);
	for(size_t i = 0; i < network->input_count; ++i)
		printf("\t\t%zu: %s\n\t\t\tSize: %zu\n", i, network->inputs[i].name, network->inputs[i].length);
	printf("\tOutput Labels:\n");
	for(size_t i = 0; i < network->output_size; ++i)
		printf("\t\t%zu: %s\n", i, network->output_labels[i]);
	printf("\tOutput Size: %zu\n", network->output_size);
	printf("\tSoftmax: %s\n", network->softmax ? "True" : "False");

	kiss_free_network(network);
	return true;
}

#ifdef WINDOWS
int wmain(int argc, wchar_t** argv)
#else
int main(int argc, char** argv)
#endif
{
	if(argc < 2) {
		printf("Usage %s [NETWORK_FILE_NAMES...]\n", argc > 0 ? "printinfo" : argv[0]);
		return 1;
	}

	for(int i = 1; i < argc; ++i) {
		print_network_info(argv[i]);
		printf("\n");
	}

	return 0;
}
