#!/bin/bash -e
#
# libkissinference - an inference libary for kiss networks
# Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
#
# This file is part of libkissinference.
#
# libkissinference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libkissinference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
#

PROJECTNAME=@PROJECT_NAME@
SYSTEMPROC=@CMAKE_SYSTEM_PROCESSOR@
ONNXNAME="@ONNX_NAME@"
VERSION="@CMAKE_PROJECT_VERSION_MAJOR@.@CMAKE_PROJECT_VERSION_MINOR@.@CMAKE_PROJECT_VERSION_PATCH@"
BINARYDIR="@CMAKE_CURRENT_BINARY_DIR@"
SRCDIR="@CMAKE_CURRENT_SOURCE_DIR@"
RELDIRECTORY="$BINARYDIR/packaged/$VERSION/release"
ZIPNAME=$PROJECTNAME-$SYSTEMPROC-$VERSION

rm $BINARYDIR/packaged/$ZIPNAME.zip || true
cd $BINARYDIR
install -d $RELDIRECTORY
cp kissinference.dll $RELDIRECTORY
cp kissinference.lib $RELDIRECTORY
cp kissinference_cli.exe $RELDIRECTORY
cp examples/kissinference_printinfo.exe $RELDIRECTORY
cp examples/kissinference_classify.exe $RELDIRECTORY
mkdir $RELDIRECTORY/networks || true
mkdir $RELDIRECTORY/include || true
cp -r $SRCDIR/kissinference $RELDIRECTORY/include
cp $SRCDIR/examples/networks/* $RELDIRECTORY/networks
cp $ONNXNAME/lib/*.dll $RELDIRECTORY
cp $SRCDIR/gpl-3.0.txt $SRCDIR/lgpl-3.0.txt $RELDIRECTORY
cp $SRCDIR/README.md $RELDIRECTORY
cd $RELDIRECTORY/..
zip -r $BINARYDIR/packaged/$ZIPNAME.zip release
