# libkissinference

_libkissinference_ is a c shared library to apply the various neural networks trained for the KISS project
_libkissinference_ is well integrated to eisgenerator, Eigen and PyTorch.

Full documentaton can be found [here](https://docs.rhd-instruments.de/kiss/kissinference/) or by building the doc target

## Building

the main devolpment platform of _libkissinference_ is linux, and _libkissinference_ works best on UNIX-like systems, however _libkissinference_ is fully portable and should compile on any platform that supports the requriements below

### Requirments
- [git](https://git-scm.com/) is requried to get the source
- a C11 compliant compiler like [gcc](https://gcc.gnu.org/)
- [cmake](https://cmake.org/) version 3.21 or later
- Microsoft [onnxruntime](https://onnxruntime.ai/)
- [Doxygen](https://www.doxygen.nl/index.html) is optional, but required to build the documentation

### Procedure
```
$ git clone https://git-ce.rwth-aachen.de/carl_philipp.klemm/libkissinference
$ mkdir libkissinference/build
$ cd libkissinference/build
$ cmake ..
$ make
$ make install
$ make doc
```

### Linking

it is best to link to this library with the help of [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/) as this provides platform a agnostic to query for paths and flags. Almost certenly, pkg-config is already integrated into your buildsystem.

# Licence

_libkissinference_ is licenced to you under the LGPL version 3 , or (at your option) any later version. see lgpl-3.0.txt or [LGPL](https://www.gnu.org/licenses/lgpl-3.0.en.html) for details
