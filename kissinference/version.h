/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

struct kiss_version_fixed
{
	int major;
	int minor;
	int patch;
};

/**
 * @brief Returns the version of libkissinference
 *
 * @return The version as a kiss_version_fixed struct, owned by libkissinference, do not free.
 */
const struct kiss_version_fixed kiss_get_version(void);

#ifdef __cplusplus
}
#endif
