/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
* API for use by applications using libkissinference-train
* @defgroup API Training API
*
* Api to finetune kiss models
* @{
*/


typedef enum {
	KISS_LOSS_NNL, /** NNL loss */
} kiss_loss_t;

typedef enum {
	KISS_STEP_COMPLETED, /**< Emmited when a training optimizer step compleates, not emmited after the final step */
	KISS_COMPLETED, /**< Emmited when a training completes the final optimizer step */
	KISS_ERROR_STATE /**< Emmited when an error is encountered */
} kiss_train_state_t;

struct kiss_train_priv;
struct kiss_network;

/**
 * @brief Struct describing a training session
 */
struct kiss_training_session {
	bool ready; /**< Set to true of the session is ready to train */
	void *user_ptr; /**< Contains user pointer that is passed to the callbacks */
	struct kiss_network *net; /**< A network struct containing network metadata, note this struct is not complete and can not be used for inference */

	struct kiss_train_priv *priv;
};

struct kiss_training_data {
	float *in; /**< Network input */
	size_t in_size; /**< Length of the input array */
	float *out; /**< Target output for loss computation */
	size_t out_size; /**< Length of the target output array */
};

/**
 * @brief A function pointer of this type is used by libkissinference-train to request data from the user
 *
 * @param session the session that this request belongs too
 * @param user_ptr the user pointer supplied to kiss_start_training
 * @return a kiss_training_data struct with the data for the next optimization step or NULL if no futher data is available
 */
typedef struct kiss_training_data *(*kiss_data_feed_cb_t)(struct kiss_training_session *session, void *user_ptr);

/**
 * @brief A function pointer of this type is used by libkissinference to free data requested by the user of this libary via a kiss_data_feed_cb_t callback
 *
 * @param data the data to be freed
 * @param user_ptr the user pointer supplied to kiss_start_training
 */
typedef void (*kiss_data_free_cb_t)(struct kiss_training_data *data, void *usr_ptr);

/**
 * @brief A function pointer of this type is used by libkissinference to inform the user of this libary
 *
 * @param data the data to be freed
 * @param user_ptr the user pointer supplied to kiss_start_training
 */
typedef void (*kiss_train_progress_cb_t)(struct kiss_training_session *session, kiss_train_state_t state, float loss, int step, void *usr_ptr);

/**
 * @brief Loads a training session archive from disk.
 *
 * If allocation fails this function will return NULL, for all other errors this function will return a
 * network with ready set to false. In this case a detailed error string can be aquired by calling kiss_train_get_strerror
 * on the network.
 * Any result, except NULL, of this function musst be freed with kiss_free_training_session().
 *
 * To avoid leaking memory, the result parameter of result_cb must be freed by result_cb or subsiquently managed by the user of this api.
 *
 * @param path A path to a training archive.
 * @param batch_size the batch size for training
 * @return a pointer to a newly allocated struct kiss_training_session to be freed with kiss_free_training_session() or NULL on allocation failure.
 */
struct kiss_training_session *kiss_create_training_session(const char* path, int batch_size);

/**
 * @brief Loads a training session archive from disk while using a session allocated by the caller
 *
 * In case of an error this function will set return false and the ready bit in the network to false.
 * In this case a detailed error string can be aquired by calling kiss_train_get_strerror.
 * The resources associated with the session created by this function musst be freed with kiss_free_training_session_prealloc().
 * The session struct itself must be freed by the caller of this function after calling kiss_free_training_session_prealloc.
 *
 * @param session A prellocated kiss_training_session struct
 * @param path A path to a training archive.
 * @param batch_size the batch size for training.
 * @return true on sucess, false on failure.
 */
bool kiss_create_training_session_prealloc(struct kiss_training_session* session, const char *path, int batch_size);


/**
 * @brief Frees a kiss_training_session.
 *
 * @param session The session to be freed.
 */
void kiss_free_training_session(struct kiss_training_session *session);


/**
 * @brief Frees the resources associated with a kiss_training_session but not the struct itself.
 *
 * @param session The session whos resources shal be freed.
 */
void kiss_free_training_session_prealloc(struct kiss_training_session *session);

/**
 * @brief Starts training the network
 *
 * @param session The session to be trained.
 * @param session The loss function to use for training.
 * @param datafeeder A callback that libkissinference can call to get data for training.
 * @param datafreeer A callback that libkissinference can call free the data gained by datafeeder.
 * @param progresscb A callback that libkissinference calls to inform the user about training progress and errors.
 * @return True if sucessfull, false otherwise. In case of failure kiss_train_get_strerror is set with a description of the error.
 */
bool kiss_start_training(struct kiss_training_session *session, kiss_loss_t loss, kiss_data_feed_cb_t datafeeder,
                         kiss_data_free_cb_t datafreeer, kiss_train_progress_cb_t progresscb, void *user_ptr);

/**
 * @brief Finalizes training of the network
 *
 * this function must be called after KISS_COMPLETED is set to the associated  kiss_train_progress_cb_t.
 *
 * @param session The session to be finalized.
 */
void kiss_finish_training(struct kiss_training_session *session);


/**
 * @brief Gets a error description string descrebing the lass orccured error
 *
 * @param session The session in which the error occured
 * @return A error description string, valid untill the next call to libkissinference, owned by libkissinference do not free.
 */
const char *kiss_train_get_strerror(struct kiss_training_session *session);

/**
 * @brief Gets a description string descrebing the state
 *
 * @param state The state to describe.
 * @return A description string, owned by libkissinference do not free.
 */
const char *kiss_state_to_str(kiss_train_state_t state);

/**
....
* @}
*/

#ifdef __cplusplus
}
#endif
