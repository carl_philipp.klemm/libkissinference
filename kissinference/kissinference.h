/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <stddef.h>
#include <stdbool.h>

#include "version.h"

#ifdef __cplusplus
extern "C" {
#endif

struct kiss_priv;

/**
* API for use by applications using libkissinference
* @defgroup API Client API
*
* Api to use kiss models
* @{
*/

/**
 * @brief A struct containing a named vector.
 */
struct kiss_vector {
	const char *name; /**< Name or description of the data this vector holds.*/
	float *values; /**< The elements of the vector.*/
	size_t length; /**< The number of elements in this vector.*/
};

/**
 * @brief Struct describing a kiss neural network
 */
struct kiss_network
{
	struct kiss_priv *priv;

	struct kiss_vector *inputs; /**< Network expects these to be given as the input.*/
	size_t input_count; /**< The number of inputs the network expects. */
	size_t output_size; /**< Size of tensor this network returns.*/
	bool *output_mask; /**< An array of booleans that set an input enabled or disabled >*/
	char *purpose; /**< A string describing the purpose of the network.*/
	char **output_labels; /**< A description string for eatch element of the output tensor.*/
	bool ready; /**< Specifies if the network is ready to use, must be checked after loading.*/
	bool softmax; /**< Network ends with a softmax.*/
	float *output_scalars; /**< Network expects outputs to be scaled by this factor to get physical values.*/
	float *output_biases; /**< Network expects outputs to be biased by this factor to get physical values.*/

	void (*result_cb)(float *result, struct kiss_network* net, void *user_data);
};

/**
 * @brief Frees the resources associated with a network
 *
 * @param net The network to free.
 */
void kiss_free_network(struct kiss_network *net);

/**
 * @brief Same as kiss_free_network except it dose not free the network struct itself
 *
 * @param net The network to free.
 */
void kiss_free_network_prealloc(struct kiss_network *net);

/**
 * @brief Loads a network from disk.
 *
 * If allocation fails this function will return NULL, for all other errors this function will return a
 * network with ready set to false. In this case a detailed error string can be aquired by calling kiss_get_strerror
 * on the network.
 * Any result, except NULL, of this function musst be freed with kiss_free_network().
 *
 * To avoid leaking memory, the result parameter of result_cb must be freed by result_cb or subsiquently managed by the user of this api.
 *
 * @param result_cb A pointer to a callback that will be called after inference is done.
 * @param path A path to a onnx network file.
 * @return a pointer to a newly allocated struct kiss_network to be freed with kiss_free_network() or NULL on allocation failure.
 */
struct kiss_network *kiss_load_network(const char *path, void (*result_cb)(float* result, struct kiss_network* net, void* user_data), bool verbose);

/**
 * @brief Allocate a vector struct of length elements and copy the given data into the vector
 *
 * @param name A label describing the vector
 * @param values The values to be copied into the vector
 * @return a pointer to a newly allocated kiss_vector struct to be freed by kiss_free_vector or NULL on OOM
 */
struct kiss_vector *kiss_create_vector(const char *name, float *values, size_t length);
bool kiss_create_vector_prealloc(struct kiss_vector *vector, const char *name, float *values, size_t length);

/**
 * @brief Frees the given vector
 *
 * @param vector the vector to be freed
 */
void kiss_free_vector(struct kiss_vector *vector);
void kiss_free_vector_prealloc(struct kiss_vector *vector);

/**
 * @brief Loads a network from disk.
 *
 * This function is the same as kiss_load_network except that a allocated network struct must be provided.
 * This is usefull where it is desireable to use a different allocator for this struct
 * on the network.
 *
 * the resources of resulting network must be freed by kiss_free_network_prealloc.
 *
 * @param net a pointer to a allocated kiss_network struct, this pointer must remain valid until after kiss_free_network_prealloc is called.
 * @param result_cb A pointer to a callback that will be called after inference is done.
 * @param path A path to a onnx network file.
 * @return true on sucess or false on failure, kiss_filter_spectra is set on failure.
 */
bool kiss_load_network_prealloc(struct kiss_network* net, const char *path, void (*result_cb)(float* result, struct kiss_network* net, void* user_data), bool verbose);

/**
 * @brief Loads a network from a memory address.
 *
 * This function is the same as kiss_load_network except it takes a in memory buffer to a onnx file instead to a path on disk.
 *
 * @param result_cb A pointer to a callback that will be called after inference is done.
 * @param buffer A pointer to a onnx file in memory
 * @param size the size of the given buffer
 * @return a pointer to a newly allocated struct kiss_network to be freed with kiss_free_network() or NULL on allocation failure.
 */
struct kiss_network *kiss_load_network_from_buffer(const char* buffer, size_t size, void (*result_cb)(float* result, struct kiss_network* net, void* user_data), bool verbose);

/**
 * @brief Loads a network from a memory address.
 *
 * This function is the same as kiss_load_network_prealloc except it takes a in memory buffer to a onnx file instead to a path on disk.
 *
 * The resources of resulting network must be freed by kiss_free_network_prealloc.
 *
 * @param net a pointer to a allocated kiss_network struct, this pointer must remain valid until after kiss_free_network_prealloc is called.
 * @param result_cb A pointer to a callback that will be called after inference is done.
 * @param buffer A pointer to a onnx file in memory
 * @param size the size of the given buffer
 * @return true on sucess or false on failure, kiss_filter_spectra is set on failure.
 */
bool kiss_load_network_from_buffer_prealloc(struct kiss_network* net, const char* buffer, size_t size, void (*result_cb)(float* result, struct kiss_network* net, void* user_data), bool verbose);

/**
 * @brief Runs inference on a network with the given inputs
 *
 * The inputs parameter is copied and may be freed immidatly after the call to this function.
 * net must remain valid until the corrisponding result_cb call is recived.
 *
 * @param inputs An array of floats, must as long as input_size for the network.
 * @param user_data A pointer that will be passed to the callback
 * @return true if the inference call was sucessfully dispatched, will return false and set an error for kiss_get_strerror() otherwise.
 */
bool kiss_async_run_inference(struct kiss_network *net, const struct kiss_vector *inputs, size_t inputs_count, void* user_data);


/**
 * @brief Sets the output mask on a given network
 *
 * This function must only be used while no inference requests are pending.
 *
 * @param net The network to set the mask on.
 * @param output_mask The output mask to set. The given array must be at least output_size in lengh
 * @return true if the mask was sucessfully set, will return false and set an error for kiss_get_strerror otherwise.
 */
bool kiss_set_output_mask(struct kiss_network *net, const bool *output_mask);

/**
 * @brief Gets the number of outputs not masked by the input mask
 *
 * @param net The network to get the active count for
 * @return the number of active outputs
 */
size_t kiss_get_active_output_count(struct kiss_network *net);

/**
 * @brief Allocates an array of floats that contins a linear or log range
 *
 * @param start The start of the range.
 * @param end The end of the range.
 * @param length The amount of elements in the range.
 * @param log If true the points in the range will be distrobuted in along a log10 spaceing, otherwise linear spaceing will be used.
 * @return A newly allocated array of floats, to be freed with free().
 */
float *kiss_create_range(float start, float end, size_t length, bool log);

/**
 * @brief Resamples a spectra, useing linear interpolation
 *
 * @param in_re The real part of the input spectra.
 * @param in_im The imaginary part of the input spectra.
 * @param input_length The length of the input spectra.
 * @param out_re A pointer where the real part of the resampled spectra will be allocated. to be freed with free().
 * @param out_im A pointer where the imaginary part of the resampled spectra will be allocated. to be freed with free().
 * @param output_length The desired length of the output spectra.
 */
void kiss_resample_spectra(float *in_re, float *in_im, size_t input_length, float **out_re, float **out_im, size_t output_length);


/**
 * @brief Normalizes the given spectra to [0,1]
 *
 * @param in_re The real part of the input spectra.
 * @param in_im The imaginary part of the input spectra.
 * @param input_length The length of the input spectra.
 */
void kiss_normalize_spectra(float *in_re, float *in_im, size_t input_length);


/**
 * @brief Reduces the data by removing "uninteresting" regions. Uninteresting regions are determined by taking the first and second derivative and removing the areas where these change very little.
 *
 * @param in_re The real part of the input spectra.
 * @param in_im The imaginary part of the input spectra.
 * @param input_length The length of the input spectra.
 * @param out_re A pointer where the real part of the reduced spectra will be allocated. to be freed with free().
 * @param out_im A pointer where the imaginary part of the reduced spectra will be allocated. to be freed with free().
 * @param output_length A pointer to a size_t where the new length of the reduced spectra will be stored.
 * @param return returns true if the spectra was reduced sucessfully, false otherwise, dose not set kiss_get_strerror
 */
bool kiss_reduce_spectra(float *in_re, float *in_im, float *omegas, size_t input_length,
                         float thresh_factor, bool use_second_deriv,
                         float **out_re, float **out_im, size_t *output_length);

/**
 * @brief Filteres the spectra by applying kiss_normalize_spectra, kiss_reduce_spectra and kiss_resample_spectra
 *
 * @param in_re The real part of the input spectra.
 * @param in_im The imaginary part of the input spectra.
 * @param input_length The length of the input spectra.
 * @param out_re A pointer where the real part of the filtered spectra will be allocated. to be freed with free().
 * @param out_im A pointer where the imaginary part of the filtered spectra will be allocated. to be freed with free().
 * @param output_length The length the spectra needs to have for the network.
 * @param return returns true if the spectra was filtered sucessfully, false otherwise, dose not set kiss_get_strerror
 */
bool kiss_filter_spectra(float *in_re, float *in_im, float *omegas, size_t input_length,
                         float **out_re, float **out_im, size_t output_length);

/**
 * @brief Calculates the element wise absolute gradient at the given point of the data given.
 *
 * @param in_re The real part of the input spectra.
 * @param in_im The imaginary part of the input spectra.
 * @param in_im The frequencies of the input spectra.
 * @param input_length The length of the input spectra.
 * @param index The index at which to aproxmiate the gradiant
 */
float *kiss_absgrad(float *in_re, float *in_im, float *omegas, size_t input_length, size_t index);

/**
 * @brief Approximates the gradiant at the given index
 *
 * @param data An array of floats on which to calculate the gradiant.
 * @param input_length The length of the input data.
 * @param index The index at which to aproxmiate the gradiant.
 */
float kiss_grad(float *data, float *omegas, size_t input_length, size_t index);

/**
 * @brief Calculates the median of the given data.
 *
 * @param data An array of floats to calculate the median on.
 * @param input_length The length of the input data.
 * @return The median.
 */
float kiss_median(float *data, size_t input_length);

/**
 * @brief Calculates the mean of the given data.
 *
 * @param data An array of floats to calculate the median on.
 * @param input_length The length of the input data.
 * @return The mean.
 */
float kiss_mean(float *data, size_t input_length);

/**
 * @brief Normalizes the given spectra to [0,1]
 *
 * Results in undefined behavior to call this function without a preceeding call to libkissinference erroring out.
 * Any subsiquent call to libkissinference invalidates the retuned string.
 *
 * @param net The net on which a call failed
 * @return A string describing the error, owned by libkissinference, do not free.
 */
const char *kiss_get_strerror(struct kiss_network *net);

/**
 * @brief Applies softmax to the given data.
 *
 * @param data An array of floats to calculate the softmax on.
 * @param input_length The length of the input data.
 */
void kiss_softmax(float *data, size_t input_length);

/**
 * @brief Applies a linear nn layer to the given input.
 *
 * @param data An array of floats to calculate linear layer on.
 * @param weight the weights of the layer.
 * @param bias the biases of the layer.
 * @param length the length of the inputs.
 */
void kiss_linear(float* data, float* weight, float* bias, size_t lengh);

/**
 * @brief Applies softmax to the given data ignoreing the inputs where mask is false.
 *
 * @param data An array of floats to calculate the softmax on.
 * @param mask An array of bools, true where the value in data shal be used and false where it shal not. Must be the same length as data.
 * @param input_length The length of the input data.
 */
void kiss_softmax_masked(float *data, bool *mask, size_t input_length);

/**
 * @brief Checks the given floats for equality with a tollerance of ulp epsilons around the sum of the inputs.
 *
 * @param a The first input.
 * @param b The second input to be compared with the first.
 * @param ulp number of epsilons of tollerance.
 * @return True if the value of b is within ulp epsilons of a, false otherwise.
 */
bool kiss_float_eq(float a, float b, unsigned int ulp);

/**
 * @brief Translates a string for this library to the format used by RHD instruments RelaxIS
 *
 * Note that any parameters embedded into the eisgenerator model string will be striped and
 * not included in the RelaxIS model.
 *
 * @param in The eisgenerator model string.
 * @return A newly allocated RelaxIS model string. To be freed with free() or NULL if translation failed
 */
char* kiss_eis_to_relaxis(const char* in);

/**
 * @brief Translates a string for this library a Boukamp Circuit Description Code (CDC).
 *
 * Note that any parameters embedded into the eisgenerator model string will be striped and
 * not included in the CDC.
 *
 * @param in The eisgenerator model string.
 * @return A newly allocated CDC string. To be freed with free() or NULL if translation failed
 */
char* kiss_eis_to_cdc(const char* in);

/**
 * @brief Frees the data with the same allocateor as libkissinference.
 *
 * @param data A pointer to the data to free
 */
void kiss_free(void *data);

void kiss_network_print_layout(struct kiss_network *net);

/**
....
* @}
*/

#ifdef __cplusplus
}
#endif


