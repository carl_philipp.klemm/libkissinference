//
// libkissinference - an inference libary for kiss networks
// Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
//
// This file is part of libkissinference.
//
// libkissinference is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// libkissinference is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
//

#include "kissinference.h"
#include <utility>
#include <string>
#include <vector>
#include <string.h>

static const struct std::pair<const char*, const char*> eisRelaxisTable[] =
{
	{"r", "R"},
	{"R", "R"},
	{"c", "C"},
	{"C", "C"},
	{"l", "I"},
	{"L", "I"},
	{"p", "P"},
	{"P", "P"},
	{"w", "W"},
	{"W", "W"},
	{"t", "Tlmo"},
	{"T", "Tlmo"},
	{nullptr, nullptr}
};

static const struct std::pair<const char*, const char*> eisCdcTable[] =
{
	{"r", "R"},
	{"R", "R"},
	{"c", "C"},
	{"C", "C"},
	{"l", "L"},
	{"L", "L"},
	{"p", "P"},
	{"P", "P"},
	{"w", "W"},
	{"W", "W"},
	{"t", "T"},
	{"T", "T"},
	{nullptr, nullptr}
};

static std::string translateElement(const std::string& in, const struct std::pair<const char*, const char*>* table, bool reverse = false)
{
	size_t i = 0;
	while(!reverse ? table[i].first : table[i].second)
	{
		if(std::string(!reverse ? table[i].first : table[i].second) == in)
			return std::string(reverse ? table[i].first : table[i].second);
		++i;
	}

	return std::string("x");
}

static bool isValidSymbol(const std::string& in, const struct std::pair<const char*, const char*>* table, bool reverse = false)
{
	size_t i = 0;
	while(!reverse ? table[i].first : table[i].second)
	{
		if(std::string(!reverse ? table[i].first : table[i].second) == in)
			return true;
		++i;
	}
	return false;
}

static void purgeEisParamBrackets(std::string& in)
{
	int bracketCounter = 0;

	std::string out;

	for(size_t i = 0; i < in.size(); ++i)
	{
		if(in[i] == '{' || in[i] == '[' || in[i] == '<')
			++bracketCounter;
		if(bracketCounter == 0)
			out.push_back(in[i]);
		if(in[i] == '}' || in[i] == ']' || in[i] == '>')
			--bracketCounter;
	}
	in = out;
}

static void balanceBrakets(std::string& in)
{
	std::string appendStr;
	for(size_t i = 0; i < in.size(); ++i)
	{
		switch(in[i])
		{
			case '(':
				appendStr.push_back(')');
				continue;
			case '[':
				appendStr.push_back(']');
				continue;
			case ')':
				appendStr.pop_back();
				continue;
			case ']':
				appendStr.pop_back();
				continue;
			default:
				continue;
		}
	}
	for(size_t i = appendStr.size(); i > 0; --i)
		in.push_back(appendStr[i-1]);
}

static std::string cdcForBacket(const std::string& in, long int bracketStart)
{
	size_t bracketEnd;
	for(bracketEnd = bracketStart+1 ; bracketEnd < in.size() && in[bracketEnd] != ')'; ++bracketEnd);

	std::string bracket = in.substr(bracketStart+1, bracketEnd-(bracketStart+1));

	bool serial = true;

	if(bracket.size() > 1 && bracket[1] != '-')
		serial = false;

	std::string out("[");
	if(!serial)
		out.push_back('(');

	for(size_t i = 0; i < bracket.size(); ++i)
	{
		if(bracket[i] == '-')
			continue;
		if(serial && i+1 < bracket.size() && bracket[i+1] != '-')
		{
			out.push_back('(');
			serial = false;
		}

		if(bracket[i] > 47 && bracket[i] < 58)
			out.push_back(bracket[i]);
		else if(isValidSymbol(std::string(1, bracket[i]), eisCdcTable))
			out.append(translateElement(std::string(1, bracket[i]), eisCdcTable));

		if(!serial && i+1 < bracket.size() && bracket[i+1] == '-' )
		{
			out.push_back(')');
			serial = true;
		}

	}

	balanceBrakets(out);

	return out;
}

static size_t getDeepestBraket(const std::string& in, size_t& maxDepth)
{
	size_t loc = 0;
	size_t depth = 0;
	maxDepth = 0;

	for(size_t i = 0; i < in.size(); ++i)
	{
		if(in[i] == '(')
		{
			++depth;
			if(depth > maxDepth)
			{
				maxDepth = depth;
				loc = i;
			}
		}
		else if(in[i] == ')')
		{
			--depth;
		}
	}
	return loc;
}

static void replaceBraket(std::string& in, size_t start, size_t num)
{
	char opposing;
	switch(in[start])
	{
		case '(':
			opposing = ')';
			break;
		case '[':
			opposing = ']';
			break;
		default:
			return;
	}

	size_t end;
	for(end = start; end < in.size() && in[end] != opposing; ++end);

	in.erase(start, (end-start)+1);
	in.insert(start, std::to_string(num));

}

static bool removeUnnededBracket(std::string& in, size_t start_index)
{
	size_t level = 0;
	for(size_t i = start_index+1; i < in.size(); ++i)
	{
		if(in[i] == '(')
		{
			++level;
		}
		else if(level == 0)
		{
			if(in[i] == '-')
				break;
			else if(in[i] == ')')
			{
				if(i - start_index > 2)
				{
					in.erase(i, 1);
					in.erase(start_index, 1);
					return true;
				}
				break;
			}
		}
		else if(in[i] == ')')
		{
			--level;
		}
	}

	return false;
}

static void removeUnnededBrackets(std::string& in)
{
	for(size_t i = 0; i < in.size(); ++i)
	{
		if(in[i] == '(' && removeUnnededBracket(in, i) && i > 0)
		{
			--i;
		}
	}
}

char* kiss_eis_to_relaxis(const char* in)
{
	char* workcstr = kiss_eis_to_cdc(in);
	std::string work = workcstr;
	free(workcstr);
	std::string out;

	size_t level = 0;

	for(size_t i = 0; i < work.size(); ++i)
	{
		if(work[i] == '[' || work[i] == '(')
		{
			out.push_back('(');
			++level;
		}
		else if(work[i] == ']' || work[i] == ')')
		{
			if(out.back() == '-')
				out.pop_back();
			out.push_back(')');
			--level;
			if(level % 2 == 0)
				out.push_back('-');
		}
		else
		{
			std::string eisElement = translateElement(std::string(1, work[i]), eisCdcTable, true);
			std::string relaxisElement = translateElement(eisElement, eisRelaxisTable, false);
			if(level % 2 == 1)
			{
				relaxisElement.push_back(')');
				relaxisElement.insert(relaxisElement.begin(), '(');
			}
			out.append(relaxisElement);
			if(level % 2 == 0)
				out.push_back('-');
		}
	}

	if(out.back() == '-')
		out.pop_back();

	removeUnnededBrackets(out);

	return strdup(out.c_str());
}

char* kiss_eis_to_cdc(const char* in)
{
	std::vector<std::string> brackets;
	std::string workString = in;
	purgeEisParamBrackets(workString);

	size_t maxDepth;
	size_t bracketStart = getDeepestBraket(workString, maxDepth);
	while(maxDepth > 0)
	{
		brackets.push_back(cdcForBacket(workString, bracketStart));
		replaceBraket(workString, bracketStart, brackets.size()-1);
		bracketStart = getDeepestBraket(workString, maxDepth);
	}

	std::string out = cdcForBacket(workString, -1);

	for(size_t i = 0; i < out.size(); ++i)
	{
		if(out[i] > 47 && out[i] < 58 && static_cast<size_t>(out[i]-48) < brackets.size())
		{
			out.insert(i+1, brackets[out[i]-48]);
			out.erase(out.begin()+i);
		}
	}

	bool paralelFirst = false;
	for(size_t i = 1; i < out.size(); ++i)
	{
		if(out[i] == '[')
		{
			paralelFirst = true;
			break;
		}
		else if(out[i] == '(')
		{
			break;
		}
	}

	if(!paralelFirst)
	{
		out.erase(out.begin());
		out.erase(out.end()-1);
	}

	return strdup(out.c_str());
}

