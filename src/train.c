/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "train.h"

#include <string.h>
#include <onnxruntime_c_api.h>
#include <onnxruntime_training_c_api.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <threads.h>
#include <assert.h>

#include "kissinference.h"
#include "util.h"
#include "version.h"
#include "microtar.h"
#include "nxjson.h"


#define KISS_CK_STATUS_TRD(func, msg, priv, label) \
{ \
	OrtStatus *stat = func; \
	if(stat) {\
		mtx_lock(&priv->err_mtx); \
		snprintf(priv->err, KISS_STRERROR_LEN, msg ": %s", api->GetErrorMessage(stat)); \
		mtx_unlock(&priv->err_mtx); \
		api->ReleaseStatus(stat); \
		priv->progresscb(session, KISS_ERROR_STATE, 0, 0, session->user_ptr); \
		goto label; \
	} \
}

struct kiss_train_priv
{
	const OrtApi *api;
	const OrtTrainingApi *tapi;
	OrtEnv *env;
	OrtTrainingSession *session;
	OrtCheckpointState *state;

	kiss_data_feed_cb_t feedercb;
	kiss_data_free_cb_t freeercb;
	kiss_train_progress_cb_t progresscb;
	_Atomic bool training;
	thrd_t train_thread;

	int batch_size;

	char err[KISS_STRERROR_LEN];
	mtx_t err_mtx;
};

struct trainfile
{
	char *checkpoint;
	char *optimizer_model;
	char *training_model;
	char *eval_model;
	char *metdata;
	size_t checkpoint_size;
	size_t optimizer_model_size;
	size_t training_model_size;
	size_t eval_model_size;
	size_t metadata_size;
};


static void free_trainfile(struct trainfile *file)
{
	if(!file)
		return;
	if(file->checkpoint)
		free(file->checkpoint);
	if(file->optimizer_model)
		free(file->optimizer_model);
	if(file->training_model)
		free(file->training_model);
	if(file->eval_model)
		free(file->eval_model);
	if(file->metdata)
		free(file->metdata);
	free(file);
}

static struct trainfile *load_trainfile(const char *path)
{
	struct trainfile *file = calloc(1, sizeof(*file));
	mtar_t tar;
	mtar_header_t tar_header;

	int ret = mtar_open(&tar, path, "r");
	if(ret) {
		mtar_close(&tar);
		free(file);
		return NULL;
	}

	ret = mtar_find(&tar, "checkpoint", &tar_header);
	if(ret)
		goto exit_failure;
	file->checkpoint_size = tar_header.size;
	file->checkpoint = malloc(tar_header.size);
	ret = mtar_read_data(&tar, file->checkpoint, tar_header.size);
	if(ret)
		goto exit_failure;

	ret = mtar_find(&tar, "optimizer_model.onnx", &tar_header);
	if(ret)
		goto exit_failure;
	file->optimizer_model_size = tar_header.size;
	file->optimizer_model = malloc(tar_header.size);
	ret = mtar_read_data(&tar, file->optimizer_model, tar_header.size);
	if(ret)
		goto exit_failure;

	ret = mtar_find(&tar, "training_model.onnx", &tar_header);
	if(ret)
		goto exit_failure;
	file->training_model_size = tar_header.size;
	file->training_model = malloc(tar_header.size);
	ret = mtar_read_data(&tar, file->training_model, tar_header.size);
	if(ret)
		goto exit_failure;

	ret = mtar_find(&tar, "eval_model.onnx", &tar_header);
	if(ret)
		goto exit_failure;
	file->eval_model_size = tar_header.size;
	file->eval_model = malloc(tar_header.size);
	ret = mtar_read_data(&tar, file->eval_model, tar_header.size);
	if(ret)
		goto exit_failure;

	ret = mtar_find(&tar, "meta.json", &tar_header);
	if(ret)
		goto exit_failure;
	file->metadata_size = tar_header.size;
	file->metdata = calloc(tar_header.size+1, 1);
	ret = mtar_read_data(&tar, file->metdata, tar_header.size);
	if(ret)
		goto exit_failure;

	return file;

	exit_failure:

	mtar_close(&tar);
	free_trainfile(file);
	return NULL;
}

static struct kiss_network *parse_meta(char* meta)
{
	const nx_json *json = nx_json_parse_utf8(meta);
	if(!json)
		return NULL;

	struct kiss_network *net = calloc(1, sizeof(*net));
	net->input_count = 1;
	net->inputs = calloc(1, sizeof(*net->inputs));

	const nx_json *entry = nx_json_get(json, "inputSize");
	if(entry->type == NX_JSON_NULL || entry->type != NX_JSON_INTEGER)
		goto error;
	net->inputs[0].length = entry->int_value;

	entry = nx_json_get(json, "outputSize");
	if(entry->type == NX_JSON_NULL || entry->type != NX_JSON_INTEGER)
		goto error;
	net->output_size = entry->int_value;

	entry = nx_json_get(json, "purpose");
	if(entry->type == NX_JSON_NULL || entry->type != NX_JSON_STRING)
		goto error;
	net->purpose = strdup(entry->text_value);

	entry = nx_json_get(json, "softmax");
	if(entry->type == NX_JSON_NULL || entry->type != NX_JSON_BOOL)
		goto error;
	net->softmax = entry->int_value;

	entry = nx_json_get(json, "inputLabel");
	if(entry->type == NX_JSON_NULL || entry->type != NX_JSON_STRING)
		goto error;
	net->inputs[0].name = strdup(entry->text_value);

	entry = nx_json_get(json, "outputLabels");
	if(entry->type == NX_JSON_NULL || entry->type != NX_JSON_ARRAY)
		goto error;
	net->output_labels = calloc(net->output_size+1, sizeof(net->output_labels));
	size_t i = 0;
	for(const nx_json *element = entry->child; element; element = element->next) {
		if(element->type != NX_JSON_STRING)
			goto error;
		net->output_labels[i] = strdup(element->text_value);
		++i;
	}

	nx_json_free(json);
	return net;

	error:
	nx_json_free(json);
	kiss_free_network(net);
	return NULL;
}

static bool check_version()
{
	struct kiss_version_fixed version = kiss_get_version();
	struct kiss_version_fixed referance = {VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH};
	if(version.major != referance.major || version.minor != referance.minor || version.patch != referance.patch)
		return false;
	return true;
}

struct kiss_training_session *kiss_create_training_session(const char* path, int batch_size)
{
	struct kiss_training_session *session = calloc(1, sizeof(*session));
	kiss_create_training_session_prealloc(session, path, batch_size);
	return session;
}

bool kiss_create_training_session_prealloc(struct kiss_training_session* session, const char *path, int batch_size)
{
	OrtSessionOptions *opts = NULL;

	session->priv = calloc(1, sizeof(*session->priv));
	const OrtApi *api = OrtGetApiBase()->GetApi(ORT_API_VERSION);
	session->priv->api = api;
	const OrtTrainingApi *tapi = session->priv->api->GetTrainingApi(ORT_API_VERSION);
	session->priv->tapi = tapi;

	int ret = mtx_init(&session->priv->err_mtx, mtx_plain);
	if(ret != thrd_success) {
		snprintf(session->priv->err, KISS_STRERROR_LEN, "Unabel to create mutex");
		return false;
	}

	if(!check_version()) {
		snprintf(session->priv->err, KISS_STRERROR_LEN, "The version of libkissinference loaded is not compatable with the version of the training libary");
		return false;
	}

	session->priv->batch_size = batch_size;

	struct trainfile *file = load_trainfile(path);
	if(!file) {
		snprintf(session->priv->err, KISS_STRERROR_LEN, "Unable to load training file from %s: file invalid", path);
		return false;
	}

	assert(file->metdata[file->metadata_size] == 0);
	session->net = parse_meta(file->metdata);

	KISS_CK_STATUS(api->CreateEnv(ORT_LOGGING_LEVEL_WARNING, "KissInferenceTraining", &session->priv->env), "Unable to load onnxruntime", session->priv, exit_failure);
	KISS_CK_STATUS(api->CreateSessionOptions(&opts), "Unabel to create session options", session->priv, exit_failure);

	KISS_CK_STATUS(tapi->LoadCheckpointFromBuffer(file->checkpoint, file->checkpoint_size, &session->priv->state), "Unable to load checkpoint", session->priv, exit_failure);
	KISS_CK_STATUS(tapi->CreateTrainingSessionFromBuffer(session->priv->env, opts, session->priv->state,
	                                                     file->training_model, file->training_model_size,
	                                                     file->eval_model, file->eval_model_size,
	                                                     file->optimizer_model, file->optimizer_model_size, &session->priv->session),
	                                                     "Unable to create training session", session->priv, exit_failure);

	free_trainfile(file);
	api->ReleaseSessionOptions(opts);
	session->ready = true;
	return true;

	exit_failure:
	free_trainfile(file);
	if(opts)
		api->ReleaseSessionOptions(opts);
	kiss_free_training_session_prealloc(session);
	session->ready = false;
	return false;
}

void kiss_free_training_session(struct kiss_training_session *session)
{
	if(session)
		kiss_free_training_session_prealloc(session);
	free(session);
}

void kiss_free_training_session_prealloc(struct kiss_training_session *session)
{
	const OrtApi *api = session->priv->api;
	const OrtTrainingApi *tapi = session->priv->tapi;

	if(session->priv->session)
		tapi->ReleaseTrainingSession(session->priv->session);
	if(session->priv->state)
		tapi->ReleaseCheckpointState(session->priv->state);
	if(session->priv->env)
		api->ReleaseEnv(session->priv->env);
	mtx_destroy(&session->priv->err_mtx);
	free(session->priv);
	free(session->net);
}

static int kiss_train_thread(void *data)
{
	struct kiss_training_session *session = data;
	const OrtApi *api = session->priv->api;
	const OrtTrainingApi *tapi = session->priv->tapi;
	float loss = 0;
	size_t step = 0;
	OrtMemoryInfo *mem_info = NULL;
	OrtRunOptions *run_options = NULL;

	KISS_CK_STATUS_TRD(api->CreateCpuMemoryInfo(OrtDeviceAllocator, OrtMemTypeDefault, &mem_info), "Unable to create allocator", session->priv, error_state);
	KISS_CK_STATUS_TRD(api->CreateRunOptions(&run_options), "unable to create run options", session->priv, error_state);

	while(true) {
		size_t local_batch_size = 0;
		struct kiss_training_data **data = calloc(session->priv->batch_size, sizeof(data));
		while(local_batch_size < session->priv->batch_size) {
			struct kiss_training_data *data_element = session->priv->feedercb(session, session->user_ptr);
			if(!data_element)
				break;
			else
				data[local_batch_size++] = data_element;
		}

		if(local_batch_size == 0)
			break;

		const int64_t shapeIn[] = {2, data[0]->in_size};
		const int64_t shapeOut[] = {2, data[0]->out_size};
		OrtValue **input_tensor_array = calloc(local_batch_size, sizeof(input_tensor_array));
		OrtValue **label_tensor_array = calloc(local_batch_size, sizeof(label_tensor_array));
		OrtValue **output_tensor_array = calloc(local_batch_size, sizeof(label_tensor_array));
		for(size_t i = 0; i < local_batch_size; ++i) {
			KISS_CK_STATUS_TRD(api->CreateTensorWithDataAsOrtValue(mem_info, data[i]->in, data[i]->in_size*sizeof(float), shapeIn, 1, ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT, &input_tensor_array[i]),
			                   "Unable to create input tensors", session->priv, step_abort);
			KISS_CK_STATUS_TRD(api->CreateTensorWithDataAsOrtValue(mem_info, data[i]->out, data[i]->out_size*sizeof(float), shapeOut, 1, ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT, &label_tensor_array[i]),
			                   "Unable to create label tensors", session->priv, step_abort);
		}

		KISS_CK_STATUS_TRD(tapi->TrainStep(session->priv->session, run_options, local_batch_size, (const OrtValue *const *)input_tensor_array, local_batch_size, output_tensor_array),
		                   "Unable to compute outputs and gradiants for step", session->priv, step_abort);
		KISS_CK_STATUS_TRD(tapi->OptimizerStep(session->priv->session, run_options),
		                   "Unable to update weights for step", session->priv, step_abort);
		KISS_CK_STATUS_TRD(tapi->LazyResetGrad(session->priv->session),
		                   "Failed to reset gradiants", session->priv, step_abort);

		for(size_t i = 0; i < local_batch_size; ++i) {
			api->ReleaseValue(input_tensor_array[i]);
			api->ReleaseValue(output_tensor_array[i]);
			api->ReleaseValue(label_tensor_array[i]);
			if(session->priv->freeercb)
				session->priv->freeercb(data[i], session->user_ptr);
		}
		free(input_tensor_array);
		free(label_tensor_array);
		free(output_tensor_array);
		free(data);

		session->priv->progresscb(session, KISS_STEP_COMPLETED, loss, step, session->user_ptr);
		++step;

		step_abort:
		for(size_t i = 0; i < local_batch_size; ++i) {
			api->ReleaseValue(input_tensor_array[i]);
			api->ReleaseValue(label_tensor_array[i]);
		}
		free(input_tensor_array);
		free(label_tensor_array);
		free(output_tensor_array);
		free(data);
		goto error_state;
	}

	api->ReleaseMemoryInfo(mem_info);
	session->priv->progresscb(session, KISS_COMPLETED, loss, step, session->user_ptr);
	return 0;

	error_state:
	if(mem_info)
		api->ReleaseMemoryInfo(mem_info);
	if(run_options)
		api->ReleaseRunOptions(run_options);
	session->priv->progresscb(session, KISS_ERROR_STATE, loss, step, session->user_ptr);
	return 1;
}

bool kiss_start_training(struct kiss_training_session *session, kiss_loss_t loss, kiss_data_feed_cb_t datafeeder, kiss_data_free_cb_t datafreeer, kiss_train_progress_cb_t progresscb, void *user_ptr)
{
	if(session->priv->training) {
		mtx_lock(&session->priv->err_mtx);
		snprintf(session->priv->err, KISS_STRERROR_LEN, "This session is already training");
		mtx_unlock(&session->priv->err_mtx);
		return false;
	}

	session->priv->training = true;
	session->priv->feedercb = datafeeder;
	session->priv->freeercb = datafreeer;
	session->priv->progresscb = progresscb;
	session->user_ptr = user_ptr;

	if(!session->priv->feedercb || !session->priv->progresscb) {
		snprintf(session->priv->err, KISS_STRERROR_LEN, "datafeeder and progresscb can not be NULL");
		return false;
	}

	int ret = thrd_create(&session->priv->train_thread, kiss_train_thread, session);
	if(ret != thrd_success) {
		session->priv->training = false;
		snprintf(session->priv->err, KISS_STRERROR_LEN, "Failed to create training thread");
		return false;
	}

	return true;
}

const char *kiss_train_get_strerror(struct kiss_training_session *session)
{
	return session ? session->priv->err : "Unable to allocate";
}

const char *kiss_state_to_str(kiss_train_state_t state)
{
	switch(state) {
		case KISS_STEP_COMPLETED:
			return "Step finished";
		case KISS_COMPLETED:
			return "Training compleated";
		case KISS_ERROR_STATE:
			return "Error";
		default:
			return "Unkown";
	}
}

