/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kissinference.h"
#include "util.h"

#include <stdlib.h>

const struct kiss_version_fixed kiss_get_version(void)
{
	static const struct kiss_version_fixed version = {VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH};
	return version;
}

void kiss_free_str_array(char **array)
{
	char **ptr = array;
	for(; *ptr; ptr++)
		free(*ptr);
	free(array);
}

void kiss_free(void *data)
{
	free(data);
}

int64_t kiss_get_tensor_size(const OrtTensorTypeAndShapeInfo *info, const struct OrtApi *api)
{
	size_t  sizes_size;
	OrtStatus *status;

	enum ONNXTensorElementDataType type;
	status = api->GetTensorElementType(info, &type);
	if(status) {
		api->ReleaseStatus(status);
		return -1;
	}
	else if(type != ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT) {
		return -1;
	}

	status = api->GetDimensionsCount(info, &sizes_size);
	if(status) {
		api->ReleaseStatus(status);
		return -1;
	}

	int64_t *sizes = malloc(sizeof(*sizes)*sizes_size);

	status = api->GetDimensions(info, sizes, sizes_size);
	if(status) {
		api->ReleaseStatus(status);
		return -1;
	}

	if(sizes_size != 2)
		return -1;
	int64_t size = sizes[1];
	free(sizes);
	return size;
}

bool kiss_check_complex(char *input_name)
{
	if(strcmp(input_name, "EIS") == 0)
		return true;
	return false;
}
