/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kissinference.h"

#include <stdlib.h>
#include <float.h>
#include <string.h>

#define _USE_MATH_DEFINES
#include <math.h>

void kiss_resample_spectra(float *in_re, float *in_im, size_t input_length, float **out_re, float **out_im, size_t output_length)
{
	*out_re = malloc(output_length*sizeof(float));
	*out_im = malloc(output_length*sizeof(float));

	for(size_t i = 0; i < output_length; ++i) {
		double position = ((double)i)/(output_length-1);
		double source_position_double = ((double)(input_length-1))*position;
		size_t source_position = (size_t)source_position_double;
		double frac = source_position_double - source_position;
		if(source_position >= input_length-1) {
			(*out_re)[i] = in_re[input_length-1];
			(*out_im)[i] = in_im[input_length-1];
		}
		else {
			(*out_re)[i] = in_re[source_position]*(1-frac) + in_re[source_position+1]*frac;
			(*out_im)[i] = in_im[source_position]*(1-frac) + in_im[source_position+1]*frac;
		}
	}
}

void kiss_normalize_spectra(float *in_re, float *in_im, size_t input_length)
{
	float maxRe = FLT_MIN;
	float maxIm = FLT_MIN;
	float minRe = FLT_MAX;
	for(size_t i = 0; i <input_length; ++i) {
		maxRe = fabsf(in_re[i]) > maxRe ? fabsf(in_re[i]) : maxRe;
		maxIm = fabsf(in_im[i]) > maxIm ? fabsf(in_im[i]) : maxIm;

		if(minRe > in_re[i])
			minRe = in_re[i];
	}

	maxRe = maxRe == minRe ? 1 : maxRe-minRe;
	maxIm = maxIm == 0 ? 1 : maxIm;

	for(size_t i = 0; i <input_length; ++i) {
		in_re[i] = (in_re[i]-minRe) / maxRe;
		in_im[i] = in_im[i] / maxIm;
	}
}

static size_t kiss_grad_index(size_t input_length, size_t index)
{
	if(index == 0)
		index = 1;
	else if(index > input_length-2)
		index = input_length-2;
	return index;
}

float *kiss_absgrad(float *in_re, float *in_im, float *omega, size_t input_length, size_t index)
{
	float *out = malloc(2*sizeof(*out));
	out[0] = 1;
	out[1] = 1;
	if(input_length < 3)
		return out;

	index = kiss_grad_index(input_length, index);

	out[0] = fabsf((in_re[index+1]-in_re[index-1])/(omega[index+1]-omega[index-1]));
	out[1] = fabsf((in_im[index+1]-in_im[index-1])/(omega[index+1]-omega[index-1]));

	return out;
}

float kiss_grad(float *data, float *omega, size_t input_length, size_t index)
{
	if(input_length < 3)
		return 0;

	index = kiss_grad_index(input_length, index);

	return (data[index+1]-data[index-1])/(omega[index+1]-omega[index-1]);
}

static int kiss_cmp_float(const void *x, const void *y)
{
	float *a = (float*)x;
	float *b = (float*)y;

	if(*a < *b)
		return 1;
	if(*b < *a)
		return -1;
	return 0;
}

float kiss_median(float *data, size_t input_length)
{
	if(input_length == 1)
		return data[0];

	float *data_cpy = malloc(input_length*sizeof(*data));
	memcpy(data_cpy, data, input_length*sizeof(*data));
	qsort(data_cpy, input_length, sizeof(*data_cpy), kiss_cmp_float);

	float retval;

	if(input_length % 2 == 0)
		retval = (data_cpy[input_length/2] + data_cpy[input_length/2-1])/2;
	else
		retval = data_cpy[input_length/2];
	free(data_cpy);
	return retval;
}

float kiss_mean(float *data, size_t input_length)
{
	double accum = 0;

	for(size_t i = 0; i < input_length; ++i)
		accum += data[i];
	return accum/input_length;
}

float *kiss_create_range(float start, float end, size_t length, bool log)
{
	float *out = malloc(sizeof(*out)*length);
	float startL = log ? log10f(start) : start;
	float endL = log ? log10f(end) : end;
	float step = (endL-startL)/(length-1);

	if(log) {
		for(size_t i = 0; i < length; ++i)
			out[i] = pow(10, step*i+log10f(start));
	}
	else {
		for(size_t i = 0; i < length; ++i)
			out[i] = start+step*i;
	}
	return out;
}

bool kiss_reduce_spectra(float *in_re, float *in_im, float *omegas, size_t input_length,
						 float thresh_factor, bool use_second_deriv,
						 float **out_re, float **out_im, size_t *output_length)
{
	if(input_length < 3)
		return false;

	kiss_normalize_spectra(in_re, in_im, input_length);

	float *grads = malloc(sizeof(*grads)*input_length);
	for(size_t i = 0; i < input_length; ++i) {
		float *grad = kiss_absgrad(in_re, in_im, omegas, input_length, i);
		grads[i] = sqrtf(pow(grad[0], 2)+pow(grad[1], 2));
		free(grad);
	}

	if(use_second_deriv) {
		for(size_t i = 0; i < input_length; ++i)
			grads[i] = fabsf(kiss_grad(grads, omegas, input_length, i));
	}

	float grad_thresh;
	if(!use_second_deriv)
		grad_thresh = kiss_median(grads, input_length)*thresh_factor;
	else
		grad_thresh = 1e-12f*thresh_factor;

	size_t start = 0;
	for(size_t i = 1; i < input_length-1; ++i) {
		if(grads[i] < grad_thresh)
			start = i;
		else
			break;
	}

	size_t end = input_length-1;
	for(size_t i = input_length-1; i > 1; --i) {
		if(grads[i] < grad_thresh)
			end = i;
		else
			break;
	}

	free(grads);

	*out_re = malloc(sizeof(float)*((end-start)+1));
	*out_im = malloc(sizeof(float)*((end-start)+1));
	*output_length = end-start+1;

	for(size_t i = 0; i < *output_length; ++i) {
		(*out_re)[i] = in_re[i+start];
		(*out_im)[i] = in_im[i+start];
	}

	return true;
}

bool kiss_filter_spectra(float *in_re, float *in_im, float *omegas, size_t input_length, float **out_re, float **out_im, size_t output_length)
{
	size_t reduce_length = 0;
	float* out_re_red = NULL;
	float* out_im_red = NULL;
	bool ret = kiss_reduce_spectra(in_re, in_im, omegas, input_length, 0.01f, false, &out_re_red, &out_im_red, &reduce_length);
	if(!ret) {
		if(out_re_red)
			free(out_re_red);
		if(out_im_red)
			free(out_im_red);
		return false;
	}

	kiss_resample_spectra(out_re_red, out_im_red, reduce_length, out_re, out_im, output_length);

	free(out_re_red);
	free(out_im_red);

	return true;
}

void kiss_softmax_masked(float *data, bool *mask, size_t input_length)
{
	float accum = 0;
	for(size_t i = 0; i < input_length; ++i) {
		if(mask[i]) {
			data[i] = powf((float)M_E, data[i]);
			accum += data[i];
		} else {
			data[i] = 0;
		}
	}

	for(size_t i = 0; i < input_length; ++i)
		data[i] /= accum;
}

void kiss_softmax(float *data, size_t input_length)
{
	bool *mask = malloc(sizeof(*mask)*input_length);
	memset(mask, true, sizeof(*mask)*input_length);
	kiss_softmax_masked(data, mask, input_length);
	free(mask);
}

void kiss_linear(float* data, float* weight, float* bias, size_t lengh)
{
	for(size_t i = 0; i < lengh; ++i)
		data[i] = (data[i]*weight[i])+bias[i];
}

bool kiss_float_eq(float a, float b, unsigned int ulp)
{
	float epsilon = (nextafterf(1.0f, INFINITY) - 1.0f)*fabs(a+b)*ulp;
	return a - epsilon <= b && a + epsilon >= b;
}

