/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <onnxruntime_c_api.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "kissinference.h"
#include "util.h"

struct kiss_priv
{
	const struct OrtApiBase *base_api;
	const struct OrtApi *api;
	OrtEnv *env;
	OrtSession *session;
	char *input_label;

	char err[KISS_STRERROR_LEN];
};

struct kiss_inference_req
{
	struct kiss_network *net;
	const char **input_names;
	const OrtValue **input_tensors;
	float *input_array;
	OrtValue *input;
	const char **output_names;
	OrtValue **output_tensors;
	void *user_data;
};

static void kiss_inference_req_free(struct kiss_inference_req *req)
{
	free(req->output_names);
	if(req->input_array)
		free(req->input_array);
	free(req->input_tensors);
	free(req->input_names);
	free(req);
}

void kiss_free_network(struct kiss_network *net)
{
	if(net) {
		kiss_free_network_prealloc(net);
		free(net);
	}
}

void kiss_free_network_prealloc(struct kiss_network *net)
{
	if(net) {
		if(net->priv) {
			if(net->priv->session)
				net->priv->api->ReleaseSession(net->priv->session);
			if(net->priv->env)
				net->priv->api->ReleaseEnv(net->priv->env);
			if(net->priv->input_label)
				free(net->priv->input_label);
			free(net->priv);
		}
		if(net->purpose)
			free(net->purpose);
		if(net->output_labels)
			kiss_free_str_array(net->output_labels);
		if(net->output_mask)
			free(net->output_mask);
		if(net->output_scalars)
			free(net->output_scalars);
		if(net->output_biases)
			free(net->output_biases);
		if(net->inputs) {
			for(size_t i = 0; i < net->input_count; ++i)
				free((void*)net->inputs[i].name);
			free(net->inputs);
		}
	}
}

static char **kiss_parse_string_list(char *output_labels, size_t *token_count)
{
	*token_count = 1;
	for(size_t i = 0; output_labels[i]; ++i) {
		if(output_labels[i] == ',')
			++(*token_count);
	}

	char **result = calloc(*token_count+1, sizeof(*result));

	char *token = strtok(output_labels, ",");
	size_t i = 0;
	do {
		result[i] = strdup(token);
		++i;
	} while((token = strtok(NULL, ",")));
	assert(i == *token_count);
	return result;
}


static float *kiss_parse_float_list(char *list, size_t *length)
{
	*length = 1;
	for(size_t i = 0; list[i]; ++i) {
		if(list[i] == ',')
			++(*length);
	}

	float *result = calloc(*length, sizeof(*result));

	char *token = strtok(list, ",");
	size_t i = 0;
	do {
		char *end;
		result[i] = strtof(token, &end);
		++i;
	} while((token = strtok(NULL, ",")));
	assert(i == *length);
	return result;
}


static long *kiss_parse_long_list(char *list, size_t *length)
{
	*length = 1;
	for(size_t i = 0; list[i]; ++i) {
		if(list[i] == ',')
			++(*length);
	}

	long *result = calloc(*length, sizeof(*result));

	char *token = strtok(list, ",");
	size_t i = 0;
	do {
		char *end;
		result[i] = strtol(token, &end, 10);
		++i;
	} while((token = strtok(NULL, ",")));
	assert(i == *length);
	return result;
}

static bool kiss_initalize_network(struct kiss_network* net, void (*result_cb)(float* result, struct kiss_network* net, void* user_data))
{
	const struct OrtApi *api = net->priv->api;
	OrtSessionOptions *opts = NULL;
	OrtModelMetadata *model_meta = NULL;
	OrtTypeInfo *type_info = NULL;
	const OrtTensorTypeAndShapeInfo *tensor_info = NULL;
	char *input_name;
	char *output_name;
	char *output_labels;
	char *output_scales;
	char *output_biases;
	char *extra_inputs;
	char *extra_input_lengths;
	char *is_softmax;
	OrtAllocator *allocator;
	size_t count;
	int64_t input_size;
	int64_t output_size;
	bool complex;
	struct kiss_vector first_input = {};
	struct kiss_vector second_input = {};

	KISS_CK_STATUS(api->SessionGetModelMetadata(net->priv->session, &model_meta), "Unable to get model metadata", net->priv, exit_failue);

	KISS_CK_STATUS(api->SessionGetInputCount(net->priv->session, &count), "Unable to get input count", net->priv, exit_failue);
	if(count != 1) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Expected model with input count 1 but got %zu", count);
		goto exit_failue;
	}

	KISS_CK_STATUS(api->GetAllocatorWithDefaultOptions(&allocator), "Unable to create allocator", net->priv, exit_failue);

	KISS_CK_STATUS(api->SessionGetInputTypeInfo(net->priv->session, 0, &type_info), "Unable to get input type info", net->priv, exit_failue);
	KISS_CK_STATUS(api->CastTypeInfoToTensorInfo(type_info, &tensor_info), "Unable to cast type_info to tensor_info", net->priv, exit_failue);
	input_size = kiss_get_tensor_size(tensor_info, api);
	api->ReleaseTypeInfo(type_info);
	if(input_size < 1) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to get input size");
		goto exit_failue;
	}
	first_input.length = input_size;

	KISS_CK_STATUS(api->SessionGetInputName(net->priv->session, 0, allocator, &input_name), "Unable to get network input name", net->priv, exit_failue);
	complex = kiss_check_complex(input_name);
	net->priv->input_label = strdup(input_name);
	if(complex) {
		const char* real = ",real";
		const char* imaginary = ",imaginary";
		first_input.name = calloc(strlen(input_name) + strlen(real) + 1, sizeof(char));
		second_input.name = calloc(strlen(input_name) + strlen(imaginary) + 1, sizeof(char));
		memcpy((char*)first_input.name, input_name, strlen(input_name));
		memcpy((char*)second_input.name, input_name, strlen(input_name));
		strcat((char*)first_input.name, real);
		strcat((char*)second_input.name, imaginary);
	} else {
		first_input.name = strdup(input_name);
	}
	allocator->Free(allocator, input_name);

	KISS_CK_STATUS(api->SessionGetOutputTypeInfo(net->priv->session, 0, &type_info), "Unable to get output type info", net->priv, exit_failue);
	KISS_CK_STATUS(api->CastTypeInfoToTensorInfo(type_info, &tensor_info), "Unable to cast type_info to tensor_info", net->priv, exit_failue);
	output_size = kiss_get_tensor_size(tensor_info, api);
	api->ReleaseTypeInfo(type_info);
	if(output_size < 1) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to get output size");
		goto exit_failue;
	}
	net->output_size = output_size;

	KISS_CK_STATUS(api->SessionGetOutputCount(net->priv->session, &count), "Unable to get output count", net->priv, exit_failue);
	if(count != 1) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Expected model with output count 1 but got %zu", count);
		goto exit_failue;
	}

	KISS_CK_STATUS(api->SessionGetOutputName(net->priv->session, 0, allocator, &output_name), "Unable to get network output name", net->priv, exit_failue);
	net->purpose = strdup(output_name);
	allocator->Free(allocator, output_name);

	KISS_CK_STATUS(api->ModelMetadataLookupCustomMetadataMap(model_meta, allocator, "outputLabels", &output_labels), "Unable to get output labels", net->priv, exit_failue);
	if(!output_labels) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "The network contains no output lables");
		goto exit_failue;
	}
	net->output_labels = kiss_parse_string_list(output_labels, &count);
	allocator->Free(allocator, output_labels);
	if(!net->output_labels || count != net->output_size) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to parse output labels, %zu != %zu", count, net->output_size);
		goto exit_failue;
	}

	OrtStatus *status = api->ModelMetadataLookupCustomMetadataMap(model_meta, allocator, "outputScalars", &output_scales);
	if(status || !output_scales) {
		if(status)
			api->ReleaseStatus(status);
		net->output_scalars = malloc(net->output_size*sizeof(*net->output_biases));
		for(size_t i = 0; i < net->output_size; ++i)
			net->output_scalars[i] = 1;
	} else {
		size_t length;
		net->output_scalars = kiss_parse_float_list(output_scales, &length);
		allocator->Free(allocator, output_scales);
		if(length != net->output_size) {
			snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to parse output biases, %zu != %zu", length, net->output_size);
			goto exit_failue;
		}
	}

	status = api->ModelMetadataLookupCustomMetadataMap(model_meta, allocator, "outputBiases", &output_biases);
	if(status || !output_biases) {
		if(status)
			api->ReleaseStatus(status);
		net->output_biases = malloc(net->output_size*sizeof(*net->output_biases));
		for(size_t i = 0; i < net->output_size; ++i)
			net->output_biases[i] = 0;
	} else {
		size_t length;
		net->output_biases = kiss_parse_float_list(output_biases, &length);
		allocator->Free(allocator, output_biases);
		if(length != net->output_size) {
			snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to parse output biases, %zu != %zu", length, net->output_size);
			goto exit_failue;
		}
	}

	status = api->ModelMetadataLookupCustomMetadataMap(model_meta, allocator, "extraInputs", &extra_inputs);

	size_t total_extra_length = 0;
	net->input_count = 1+complex;
	net->inputs = calloc(net->input_count, sizeof(*net->inputs));

	if(status) {
		api->ReleaseStatus(status);
	} else if(extra_inputs) {
		status = api->ModelMetadataLookupCustomMetadataMap(model_meta, allocator, "extraInputLengths", &extra_input_lengths);
		if(status || !extra_input_lengths) {
			allocator->Free(allocator, extra_inputs);
			if(status)
				api->ReleaseStatus(status);
			snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to get extra input lengths");
			goto exit_failue;
		} else {
			size_t inputs_length;
			size_t lengths_length;
			char **parsed_extra_inputs = kiss_parse_string_list(extra_inputs, &inputs_length);
			long *parsed_extra_input_lengths = kiss_parse_long_list(extra_input_lengths, &lengths_length);
			if(inputs_length != lengths_length) {
				allocator->Free(allocator, extra_inputs);
				allocator->Free(allocator, extra_input_lengths);
				kiss_free_str_array(parsed_extra_inputs);
				free(parsed_extra_input_lengths);
				snprintf(net->priv->err, KISS_STRERROR_LEN, "extraInputs and extraInputLengths must be arrays of the same length but, %zu != %zu", inputs_length, lengths_length);
				goto exit_failue;
			}
			net->input_count = inputs_length+1+complex;
			free(net->inputs);
			net->inputs = calloc(net->input_count, sizeof(*net->inputs));

			for(size_t i = 0; i < lengths_length; ++i) {
				net->inputs[i+1+complex].name = parsed_extra_inputs[i];
				net->inputs[i+1+complex].length = parsed_extra_input_lengths[i];
				total_extra_length += parsed_extra_input_lengths[i];
			}

			free(parsed_extra_inputs);
			free(parsed_extra_input_lengths);
			allocator->Free(allocator, extra_inputs);
			allocator->Free(allocator, extra_input_lengths);
		}
	}

	first_input.length = (first_input.length - total_extra_length)/2;
	net->inputs[0] = first_input;
	if(complex) {
		second_input.length = first_input.length;
		net->inputs[1] = second_input;
	}

	KISS_CK_STATUS(api->ModelMetadataLookupCustomMetadataMap(model_meta, allocator, "softmax", &is_softmax), "Unable to get softmax state", net->priv, exit_failue);
	if(!is_softmax) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to get softmax state");
		goto exit_failue;
	}

	if(strcmp(is_softmax, "False") == 0) {
		net->softmax = false;
	}
	else if(strcmp(is_softmax, "True") == 0) {
		net->softmax = true;
	}
	else {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to parse softmax state got: %s expected \"True\" or \"False\"", is_softmax);
		allocator->Free(allocator, is_softmax);
		goto exit_failue;
	}
	allocator->Free(allocator, is_softmax);

	net->output_mask = malloc(sizeof(*net->output_mask)*net->output_size);
	memset(net->output_mask, true, sizeof(*net->output_mask)*net->output_size);

	api->ReleaseSessionOptions(opts);
	api->ReleaseModelMetadata(model_meta);
	net->ready = true;
	return net->ready;

	exit_failue:
	if(net->priv->env)
		api->ReleaseEnv(net->priv->env);
	net->priv->env = NULL;
	if(net->priv->session)
		api->ReleaseSession(net->priv->session);
	net->priv->session = NULL;
	if(opts)
		api->ReleaseSessionOptions(opts);
	if(model_meta)
		api->ReleaseModelMetadata(model_meta);
	return net->ready;
}

bool kiss_load_network_prealloc(struct kiss_network* net, const char *path, void (*result_cb)(float* result, struct kiss_network* net, void* user_data), bool verbose)
{
	memset(net, 0, sizeof(*net));

	OrtSessionOptions *opts = NULL;

	net->priv = calloc(1, sizeof(*net->priv));
	net->priv->base_api = OrtGetApiBase();
	net->priv->api = net->priv->base_api->GetApi(ORT_API_VERSION);
	net->result_cb = result_cb;

	const struct OrtApi *api = net->priv->api;

	if(verbose)
		printf("Inialized onnxruntime %s\n", net->priv->base_api->GetVersionString());

	KISS_CK_STATUS(api->CreateEnv(verbose ? ORT_LOGGING_LEVEL_VERBOSE : ORT_LOGGING_LEVEL_WARNING, "KissInference", &net->priv->env), "Unable to load onnxruntime", net->priv, exit_failue);
	KISS_CK_STATUS(api->CreateSessionOptions(&opts), "", net->priv, exit_failue);
	KISS_CK_STATUS(api->CreateSession(net->priv->env, path, opts, &net->priv->session), "Unable create session", net->priv, exit_failue);

	api->ReleaseSessionOptions(opts);
	kiss_initalize_network(net, result_cb);

	return net->ready;

	exit_failue:
	if(net->priv->env)
		api->ReleaseEnv(net->priv->env);
	net->priv->env = NULL;
	if(net->priv->session)
		api->ReleaseSession(net->priv->session);
	net->priv->session = NULL;
	if(opts)
		api->ReleaseSessionOptions(opts);
	return net->ready;
}

struct kiss_network *kiss_load_network(const char *path, void (*result_cb)(float*, struct kiss_network*, void*), bool verbose)
{
	struct kiss_network *net = calloc(1, sizeof(*net));
	kiss_load_network_prealloc(net, path, result_cb, verbose);
	return net;
}


bool kiss_load_network_from_buffer_prealloc(struct kiss_network* net, const char* buffer, size_t size, void (*result_cb)(float* result, struct kiss_network* net, void* user_data), bool verbose)
{
	memset(net, 0, sizeof(*net));

	OrtSessionOptions *opts = NULL;

	net->priv = calloc(1, sizeof(*net->priv));
	net->priv->base_api = OrtGetApiBase();
	net->priv->api = net->priv->base_api->GetApi(ORT_API_VERSION);
	net->result_cb = result_cb;

	const struct OrtApi *api = net->priv->api;

	if(verbose)
		printf("Inialized onnxruntime %s\n", net->priv->base_api->GetVersionString());

	KISS_CK_STATUS(api->CreateEnv(verbose ? ORT_LOGGING_LEVEL_VERBOSE : ORT_LOGGING_LEVEL_WARNING, "KissInference", &net->priv->env), "Unable to load onnxruntime", net->priv, exit_failue);
	KISS_CK_STATUS(api->CreateSessionOptions(&opts), "", net->priv, exit_failue);
	KISS_CK_STATUS(api->CreateSessionFromArray(net->priv->env, buffer, size, opts, &net->priv->session), "Unable create session", net->priv, exit_failue);

	kiss_initalize_network(net, result_cb);

	return net->ready;

	exit_failue:
	if(net->priv->env)
		api->ReleaseEnv(net->priv->env);
	net->priv->env = NULL;
	if(net->priv->session)
		api->ReleaseSession(net->priv->session);
	net->priv->session = NULL;
	if(opts)
		api->ReleaseSessionOptions(opts);
	return net->ready;
}

struct kiss_network *kiss_load_network_from_buffer(const char* buffer, size_t size, void (*result_cb)(float* result, struct kiss_network* net, void* user_data), bool verbose)
{
	struct kiss_network *net = calloc(1, sizeof(*net));
	kiss_load_network_from_buffer_prealloc(net, buffer, size, result_cb, verbose);
	return net;
}

static void kiss_run_cb(void *user_data, OrtValue **outputs, size_t num_outputs, OrtStatusPtr status)
{
	struct kiss_inference_req *req = user_data;
	struct kiss_network *net = req->net;
	const struct OrtApi *api = net->priv->api;
	void *kiss_user_data = req->user_data;

	if(status) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "%s", api->GetErrorMessage(status));
		net->result_cb(NULL, net, kiss_user_data);
		api->ReleaseValue(req->input);
		kiss_inference_req_free(req);
		api->ReleaseStatus(status);
		return;
	}

	if(num_outputs != 1) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Expected one output but got %zu", num_outputs);
		net->result_cb(NULL, net, kiss_user_data);
	}

	if(outputs) {
		OrtTensorTypeAndShapeInfo *info;
		OrtStatus *status;
		status = api->GetTensorTypeAndShape(outputs[0], &info);

		if(status) {
			snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to get tensor type and shape: %s\n", api->GetErrorMessage(status));
			api->ReleaseStatus(status);
			net->result_cb(NULL, net, kiss_user_data);
		}

		if (net->output_size != kiss_get_tensor_size(info, api)) {
			snprintf(net->priv->err, KISS_STRERROR_LEN, "Output size and inference tensor result size are not the same");
			net->result_cb(NULL, net, kiss_user_data);
		}
		float *data;

		status = api->GetTensorMutableData(outputs[0], (void**)&data);
		assert(!status);

		float *output_floats = malloc(sizeof(*output_floats)*net->output_size);
		memcpy(output_floats, data, net->output_size*sizeof(*data));

		puts("Raw outptus:");
		for(size_t i = 0; i < net->output_size; ++i)
			printf("%f\n", output_floats[i]);

		if(net->softmax)
			kiss_softmax_masked(output_floats, net->output_mask, net->output_size);

		kiss_linear(output_floats, net->output_scalars, net->output_biases, net->output_size);

		api->ReleaseTensorTypeAndShapeInfo(info);

		for(size_t i = 0; i < num_outputs; ++i)
			api->ReleaseValue(outputs[i]);
		free(outputs);

		net->result_cb(output_floats, net, kiss_user_data);
	}
	else {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Could not perform inference: %s\n", api->GetErrorMessage(status));
		api->ReleaseStatus(status);
		net->result_cb(NULL, net, kiss_user_data);
	}

	api->ReleaseValue(req->input);
	kiss_inference_req_free(req);
}


static const struct kiss_vector *kiss_find_input(const struct kiss_vector *inputs, size_t inputs_count, const char *name)
{
	for(size_t i = 0; i < inputs_count; ++i) {
		if(strcmp(inputs[i].name, name) == 0)
			return &inputs[i];
	}
	return NULL;
}

bool kiss_async_run_inference(struct kiss_network *net, const struct kiss_vector *inputs, size_t inputs_count, void* user_data)
{
	const struct OrtApi *api = net->priv->api;
	if(!api) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "kissinference not ready\n");
		return false;
	}

	if(inputs_count < net->input_count) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "This network requires %zu inputs, but only %zu inputs where provided\n", net->input_count, inputs_count);
		return false;
	}

	size_t input_length = 0;
	for(size_t i = 0; i < net->input_count; ++i) {
		const struct kiss_vector *input = kiss_find_input(inputs, inputs_count, net->inputs[i].name);
		if(!input) {
			snprintf(net->priv->err, KISS_STRERROR_LEN, "This network requires the input %s, but it was not provided\n", net->inputs[i].name);
			return false;
		}
		input_length += input->length;
	}

	OrtStatus *status;

	OrtMemoryInfo *mem_info;
	status = api->CreateCpuMemoryInfo(OrtDeviceAllocator, OrtMemTypeDefault, &mem_info);
	if(status) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to create allocator: %s\n", api->GetErrorMessage(status));
		api->ReleaseStatus(status);
		return false;
	}

	OrtValue *input_tensor;
	const int64_t shape[] = {1, input_length};
	float *input_array = malloc(sizeof(*input_array)*(input_length));
	size_t offset = 0;
	for(size_t i = 0; i < net->input_count; ++i) {
		const struct kiss_vector *input = kiss_find_input(inputs, inputs_count, net->inputs[i].name);
		memcpy(input_array+offset, input->values, sizeof(*input_array)*input->length);
		offset += input->length;
	}

	status = api->CreateTensorWithDataAsOrtValue(mem_info, input_array, input_length*sizeof(float), shape, 2, ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT, &input_tensor);
	if(status) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to create tensor: %s\n", api->GetErrorMessage(status));
		api->ReleaseStatus(status);
		api->ReleaseMemoryInfo(mem_info);
		free(input_array);
		return false;
	}

	struct kiss_inference_req *req = malloc(sizeof(*req));
	req->input_names = malloc(sizeof(char*));
	req->input_names[0] = net->priv->input_label;
	req->input_tensors = malloc(sizeof(OrtValue*));
	req->input_tensors[0] = input_tensor;
	req->output_names = malloc(sizeof(char*));
	req->output_names[0] = net->purpose;
	req->output_tensors = calloc(sizeof(OrtValue*), 1);
	req->input = input_tensor;
	req->net = net;
	req->user_data = user_data;
	req->input_array = input_array;

	status = api->RunAsync(net->priv->session, NULL,
		req->input_names, req->input_tensors, 1,
		req->output_names, 1, req->output_tensors,
		kiss_run_cb, req);
	if(status) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "Unable to create tensor: %s\n", api->GetErrorMessage(status));
		api->ReleaseStatus(status);
		api->ReleaseMemoryInfo(mem_info);
		kiss_inference_req_free(req);
		return false;
	}

	api->ReleaseMemoryInfo(mem_info);

	return true;
}

bool kiss_set_output_mask(struct kiss_network *net, const bool *output_mask)
{
	if(net->output_size < 3 || !net->softmax) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "This network dose not support output masks");
		return false;
	}

	memcpy(net->output_mask, output_mask, sizeof(bool)*net->output_size);

	size_t active_count = kiss_get_active_output_count(net);

	if(active_count < 2 && active_count < net->output_size) {
		snprintf(net->priv->err, KISS_STRERROR_LEN, "At least two outputs must remain active");
		memset(net->output_mask, true, sizeof(bool)*net->output_size);
		return false;
	}

	return true;
}

size_t kiss_get_active_output_count(struct kiss_network *net)
{
	size_t count = 0;
	for(size_t i = 0; i < net->output_size; ++i)
		if(net->output_mask[i])
			++count;
	return count;
}

struct kiss_vector *kiss_create_vector(const char *name, float *values, size_t length)
{
	struct kiss_vector *out = calloc(1, sizeof(*out));
	kiss_create_vector_prealloc(out, name, values, length);
	return out;
}

bool kiss_create_vector_prealloc(struct kiss_vector *vector, const char *name, float *values, size_t length)
{
	vector->name = strdup(name);
	vector->values = malloc(sizeof(*vector->values)*length);
	memcpy(vector->values, values, sizeof(*values)*length);
	vector->length = length;
	return true;
}

void kiss_free_vector(struct kiss_vector *vector)
{
	if(vector) {
		kiss_free_vector_prealloc(vector);
		free(vector);
	}
}

void kiss_free_vector_prealloc(struct kiss_vector *vector)
{
	if(vector) {
		if(vector->name)
			free((char*)vector->name);
		if(vector->values)
			free(vector->values);
	}
}

const char *kiss_get_strerror(struct kiss_network *net)
{
	return net ? net->priv->err : "Unable to allocate";
}

void kiss_network_print_layout(struct kiss_network *net)
{
	printf("Network %p\n", net);
	printf("\tinputs %p\n", net->inputs);
	for(size_t i = 0; i < net->input_count; ++i)
		printf("\t\t%zu \"%s\": %p\n", i, net->inputs[i].name, &net->inputs[i]);
	printf("\toutput_mask %p\n", net->output_mask);
	printf("\tpurpose %p \"%s\"\n", net->purpose, net->purpose);
	printf("\toutput_labels %p\n", net->output_labels);
	for(size_t i = 0; i < net->output_size; ++i)
		printf("\t\t%zu \"%s\": %p\n", i, net->output_labels[i], &net->output_labels[i]);
	printf("\toutput_scalars %p\n", net->output_scalars);
	printf("\toutput_biases %p\n", net->output_biases);
	printf("\tresult_cb %p\n", net->result_cb);
	fflush(stdout);
}
