//
// libkissinference - an inference libary for kiss networks
// Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
//
// This file is part of libkissinference.
//
// libkissinference is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// libkissinference is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
//

#include "eistype.h"
#include <iostream>
#include <eisgenerator/model.h>
#include <eisgenerator/normalize.h>
#include <eisgenerator/basicmath.h>
#include <eisgenerator/log.h>
#include <condition_variable>
#include <mutex>
#include <cassert>
#include <type_traits>

#include <kissinference/kissinference.h>

static void result_cb(float* results, struct kiss_network* net, void *data)
{
	puts("Result:");

	std::vector<std::pair<std::string, float>> resultsVec;
	for(size_t i = 0; i < net->output_size; ++i)
		resultsVec.push_back({net->output_labels[i], results[i]});
	std::sort(resultsVec.begin(), resultsVec.end(), [](std::pair<std::string, float> a, std::pair<std::string, float> b){return a.second > b.second;});
	for(size_t i = 0; i < net->output_size; ++i)
		std::cout<<i<<'\t'<<resultsVec[i].first<<'\t'<<resultsVec[i].second<<'\n';

	free(results);
	std::condition_variable* cond = reinterpret_cast<std::condition_variable*>(data);
	cond->notify_all();
}

int main(int argc, char** argv)
{
	eis::Log::level = eis::Log::ERROR;
	if(argc < 3)
	{
		std::cout<<"Usage: "<<argv[0]<<" [MODEL] [ONNX FILE]\n";
		return 1;
	}
	bool same = std::is_same<fvalue, float>::value;
	assert(same);

	std::condition_variable cond;
	std::mutex mutex;

	struct kiss_network *net = kiss_load_network(argv[2], result_cb, false);
	if(!net)
	{
		std::cout<<"Unable to alloc memory for network"<<'\n';
		return 3;
	}
	if(!net->ready)
	{
		std::cout<<kiss_get_strerror(net)<<'\n';
		kiss_free_network(net);
		return 2;
	}

	eis::Model model(argv[1], 100, false);
	eis::Range omega = eis::Range(1, 1e6, 50, false);
	std::vector<eis::DataPoint> data = model.executeSweep(omega);

	std::pair<std::valarray<fvalue>, std::valarray<fvalue>> arrays = eis::eisToValarrays(data);
	std::vector<fvalue> re(std::begin(arrays.first), std::end(arrays.first));
	std::vector<fvalue> im(std::begin(arrays.second), std::end(arrays.second));

	kiss_normalize_spectra(re.data(), im.data(), re.size());

	float *re_filtered;
	float *im_filtered;
	size_t filtered_size;

	kiss_reduce_spectra(re.data(), im.data(), omega.getRangeVector().data(), re.size(), 0.01, false, &re_filtered, &im_filtered, &filtered_size);

	float *re_filtered_resampled;
	float *im_filtered_resampled;
	kiss_resample_spectra(re_filtered, im_filtered, filtered_size, &re_filtered_resampled, &im_filtered_resampled, net->inputs[0].length);
	free(re_filtered);
	free(im_filtered);

	std::unique_lock<std::mutex> lk(mutex);
	struct kiss_vector inputs[2] = {{"real", re_filtered_resampled, net->inputs[0].length}, {"imaginary", im_filtered_resampled, net->inputs[0].length}};
	int ret = kiss_async_run_inference(net, inputs, 2, &cond);
	if(!ret)
	{
		puts(kiss_get_strerror(net));
		return 2;
	}

	cond.wait(lk);

	free(re_filtered_resampled);
	free(im_filtered_resampled);
	kiss_free_network(net);

	return 0;
}
