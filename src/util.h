/* * libkissinference - an inference libary for kiss networks
 * Copyright (C) 2024 Carl Philipp Klemm <carl@uvos.xyz>
 *
 * This file is part of libkissinference.
 *
 * libkissinference is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libkissinference is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libkissinference.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <stdbool.h>
#include <onnxruntime_c_api.h>
#include <stdio.h>

#define KISS_STRERROR_LEN 1024

#define KISS_CK_STATUS(func, msg, priv, label) \
	{ \
		OrtStatus *stat = func; \
		if(stat) {\
			snprintf(priv->err, KISS_STRERROR_LEN, msg ": %s", api->GetErrorMessage(stat)); \
			api->ReleaseStatus(stat); \
			goto label; \
		} \
	}

void kiss_free_str_array(char **array);

int64_t kiss_get_tensor_size(const OrtTensorTypeAndShapeInfo *info, const struct OrtApi *api);

bool kiss_check_complex(char *input_name);
